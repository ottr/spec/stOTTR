.PHONY: examples all

ANTLR_VER = antlr-4.7.1-complete
ANTLR_JAR = https://www.antlr.org/download/$(ANTLR_VER).jar

JAVACP = -cp antlr.jar:java
JAVA = java $(JAVACP)
JAVAC = javac $(JAVACP)

ANTLR4 = $(JAVA) org.antlr.v4.Tool
GRUN = $(JAVA) org.antlr.v4.gui.TestRig
TESTER = antlr-tester.sh

all: examples

antlr.jar:
	wget $(ANTLR_JAR) -O $@

java: stOTTR.g4 Turtle.g4 antlr.jar
	mkdir -p $@
	touch $@ # update timestamp
	$(ANTLR4) $< -o $@
	$(JAVAC) $@/*.java -d $@

clean:
	rm antlr.jar
	rm -Rf java


example/%.txt: example/%.stottr stOTTR.g4 Turtle.g4
	bash $(TESTER) stOTTR.g4 $< stOTTRDoc > $@
	cat $@


EXAMPLES = $(wildcard example/good/*.stottr) $(wildcard example/bad/*.stottr)

examples: $(EXAMPLES:.stottr=.txt)


